<?php

namespace Equipment;

use DateTime;

class EquipmentAvailabilityHelper
{

    /**
     * This function checks if a given quantity is available in the passed time frame
     * @param int $equipment_id Id of the equipment item
     * @param int $quantity How much should be available
     * @param DateTime $start Start of time window
     * @param DateTime $end End of time window
     * @return bool True if available, false otherwise
     */
    public function isAvailable(int $equipment_id, int $quantity, DateTime $start, DateTime $end): bool
    {
        // quantity can't be smaller or equal zero
        if ($quantity <= 0) {
            throw new \InvalidArgumentException(
                sprintf('invalid quantity is given: %d', $quantity)
            );
        }
        // check if the given id exists
        // if it does, then find the total availability
        // if the given quantity is more that, return false immediately
        $totalAvailability = Equipment::find($equipment_id);
        if (!$totalAvailability) {
            throw new \RuntimeException(
                sprintf('given equipment by id: %d does not exist.', $equipment_id)
            );
        }

        // check if the quantity is valid
        if (
            $quantity > $totalAvailability->stock ||
            $totalAvailability->stock == 0) {
            return false;
        }

        // find the previous planned counts
        // dynamic local scope model function
        $planned = Planning::available($equipment_id, $start, $end)->sum('quantity');
        return ($totalAvailability->stock - $planned) >= $quantity;
    }

    /**
     * Calculate all items that are short in the given period
     * @param DateTime $start Start of time window
     * @param DateTime $end End of time window
     * @return array Key/value array with as indices the equipment id's and as values the shortages
     */
    public function getShortages(DateTime $start, DateTime $end): array
    {
        $shortagesList = Planning::shortages($start, $end)->get()->toArray();
        // if data is massive, we can use do below actions by
        // split it into chunks
        // then for each chunk, apply below
        $ids = array_column($shortagesList, 'id');
        $shortages = array_column($shortagesList, 'shortage');

        return array_combine($ids, $shortages);
    }
}
