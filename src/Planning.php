<?php

namespace Equipment;

use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{

    public $timestamps = false;
    protected $table = 'planning';
    protected $guarded = [];

    /**
     * @param Builder $builder
     * @param DateTime $start
     * @param DateTime $end
     * @return Builder
     */
    public function scopeShortages(Builder $builder, DateTime $start, DateTime $end): Builder
    {
        return $builder->join('equipment', function ($join) use ($start, $end) {
            $join->on('equipment.id', '=', 'planning.equipment')
                ->where('start', '>=', $start)
                ->where('end', '<=', $end);
        })
            ->selectRaw('
                 equipment.id as id,
                 equipment.stock - sum(planning.quantity) as shortage
             ')
            ->groupBy('equipment.id')
            ->havingRaw('shortage < 0');
    }

    /**
     * Check the equipment availability
     *
     * @param Builder $builder
     * @param int $equipment_id
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return Builder
     */
    public function scopeAvailable(Builder $builder, int $equipment_id, DateTime $start, DateTime $end): Builder
    {
        return $builder->where(function (Builder $query) use ($equipment_id, $start, $end) {
            $query->where('equipment', $equipment_id);
            $query->where('start', '>=', $start);
            $query->where('end', '<=', $end);
        });
    }

}