<?php

namespace Equipment;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'equipment';
}