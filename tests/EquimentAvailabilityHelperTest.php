<?php

namespace Tests;

use DateTime;
use Equipment\Equipment;
use Equipment\EquipmentAvailabilityHelper as EquipmentAvailability;
use Equipment\Planning;
use PHPUnit\Framework\TestCase;


class EquipmentAvailabilityHelperTest extends TestCase
{
    /** @test */
    public function is_available_function_throws_exception_if_equipment_id_is_not_found()
    {
        $start = new DateTime('2019-05-05');
        $end = new DateTime('2019-05-10');
        $equipment = new EquipmentAvailability();

        $this->expectException(\RuntimeException::class);
        $equipment->isAvailable(100000, 3, $start, $end);
    }

    /** @test */
    public function is_available_function_throws_exception_if_quantity_is_not_valid()
    {
        $start = new DateTime('2019-05-05');
        $end = new DateTime('2019-05-10');
        $equipment = new EquipmentAvailability();

        $this->expectException(\InvalidArgumentException::class);
        $equipment->isAvailable(1, -1, $start, $end);
    }

    /** @test */
    public function is_available_function_returns_false_if_equipment_total_availability_is_less_than_requested()
    {
        // create a equipment
        $eq = Equipment::create([
            'name' => 'test name',
            'stock' => 3
        ]);
        $start = new DateTime('2019-05-05');
        $end = new DateTime('2019-05-10');
        $equipment = new EquipmentAvailability();

        $result = $equipment->isAvailable($eq->id, 4, $start, $end);
        $this->assertFalse($result);
    }

    public function is_available_returns_true_if_equipement_has_enough_stock()
    {
        // create a equipment
        $eq = Equipment::create([
            'name' => 'test name 2',
            'stock' => 10
        ]);
        $start = new DateTime('2019-05-05');
        $end = new DateTime('2019-05-10');
        $equipment = new EquipmentAvailability();

        $result = $equipment->isAvailable($eq->id, 8, $start, $end);
        $this->assertTrue($result);
    }

    /** @test */
    public function can_get_shortage()
    {
        $eq = Equipment::create([
            'name' => 'test name 3',
            'stock' => 10
        ]);

        $planner1 = Planning::insert([
            'equipment' => $eq->id,
            'quantity' => 4,
            'start' => '2019-05-05',
            'end' => '2019-05-31',
        ]);

        $planner2 = Planning::insert([
            'equipment' => $eq->id,
            'quantity' => 8,
            'start' => '2019-05-11',
            'end' => '2019-05-28',
        ]);

        $start = new DateTime('2019-05-05');
        $end = new DateTime('2019-05-31');
        $availability = new EquipmentAvailability();
        $shortages = $availability->getShortages($start, $end);

        $this->assertSame(-2, (int)$shortages[$eq->id]);
    }

}