<?php

include_once "vendor/autoload.php";

use Equipment\Database;

// Loads environment variables from .env
$env = Dotenv\Dotenv::createImmutable('.');
$env->load();


// Load database configuration
$database = new Database();
$database->registerConfiguration();
