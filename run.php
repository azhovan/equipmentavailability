<?php

require __DIR__ . '/bootstrap.php';

use Equipment\EquipmentAvailabilityHelper;

try {
    // Below is an example of to use it
    $start = new DateTime('2019-05-05');
    $end   = new DateTime('2019-05-10');
    $equipmentAvailability = new EquipmentAvailabilityHelper();

    // Check the availability
    $isAvailable = $equipmentAvailability->isAvailable(1, 9, $start, $end);

    if ($isAvailable) {
        echo 'YES, equipment is available.' . PHP_EOL;
    } else {
        echo 'NO, equipment is not available.' . PHP_EOL;
    }

    // Get the shortages
    $list = $equipmentAvailability->getShortages(
        new DateTime('2019-04-02'), new DateTime('2019-04-26')
    );

    print_r(['result' => $list]);

} catch (Throwable $exception) {
    print_r($exception->getMessage());
}
