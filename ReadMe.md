#### Application configuration
This app is using `.env` file to define it's configurations. you can find it in the root of the project.
```bash
DB_CONNECTION=mysql
DB_DATABASE=assessment
DB_HOST=localhost
DB_USERNAME=root
DB_PASSWORD=18523137
DB_PORT=3306
```

#### How to run

In root of the project, run below commands: 
 
- `composer install && composer dump-autoload -o`
- `$(which php) run.php`

#### Run tests
In root of the project, run `vendor/bin/phpunit tests`

#### Optimizations
- Changing the `id` of **equipment** table and **planning** table from `int` to `bigint`
- Create compound index in planning table (to speed up queries).
To calculate efficiency here, in mysql console run `SET profiling = 1`, then run the queries, and then run `SHOW PROFILES;` 
find the queries you want and execution time.
 
```sql
create index planning_start_end_index
    on planning (start, end)
```


- Adding foreign key constraint to the planning table (to prevent actions that would destroy links between tables.)
```sql

alter table planning
    add constraint planning_equipment_id_fk
        foreign key (equipment) references equipment (id);
```

#### How it works 
- Source codes are inside the `src` directory.
- Interacting with database are done through Models (`src/Planning`, `src/Equipment`).
- an example is provided in `run.php` 


for more information check unit tests (tests/ folder)
